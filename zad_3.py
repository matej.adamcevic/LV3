# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 21:20:30 2015

@author: matej
"""

import urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np

plt.close("all")
# url that contains valid xml file:
urla='http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.01.2014&vrijemeDo=31.12.2014'

airQualityHR = urllib.request.urlopen(urla).read()  #za python v3.x treba dodati .request prije .urlopen
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme');

#add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['date'] = df['vrijeme'].dt.date      #nije bilo potrebno
df['dayOfweek'] = df['vrijeme'].dt.dayofweek
dfs=df.sort('mjerenje')
maxes=dfs[-3:]      #sortirano je tako da su na kraju popisa dani s najvecim koncentracijama lebdecih cestica
print("Dani s najvećim koncentracijama: \n",maxes)

plt.figure(2)
for i in range (1, len(df)):        #prikaz diskretnih vrijednosti po danu koristeci bar - vidljivi dani bez podataka
    plt.bar(df['date'][i], df['mjerenje'][i])  #horizontalno prosiriti prozor za bolju sliku
plt.title('Izmjerene dnevne koncentracije PM10 cestica za 2014.')    
plt.ylabel('mikrograma / m^3')

radni=df[df['dayOfweek']<=4].mjerenje   #radni dani dayOfweek = 0-4
vikend=df[df['dayOfweek']>4].mjerenje   #vikend dayOfweek = 5-6
data1=[radni, vikend]

plt.figure(4)
plt.boxplot(data1)              #distribucija koncentracija
plt.title('Distribucija koncentracija za radne dane(1) i vikende(2)')
plt.ylabel('mikrograma / m^3')
###########################################################
### kod za prikaz distribucije jednog ljetnog i jednog zimskog
### mjeseca; kod je isti kao za distribuciju radnim danima/vikendima ali
### izbacuje parse error,  "numpy.ndarray' object has no attribute 'find' "
### strukture aug i jan varijabli su iste kao i strukture varijabli radni 
### i vikend
#
#
#jan=df[df['month']==1].mjerenje
#aug=df[df['month']==8].mjerenje 
#data2=[jan, aug]
#plt.figure(3)
#plt.boxplot(data2)
#plt.title('Distribucija koncentracija za sijecanj(1) i kolovoz(2) 2014')