# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 09:04:36 2015

@author: matej
"""


import pandas as pd
import matplotlib.pyplot as plt
#import matplotlib.pyplot.xticks as xticks
import numpy as np

plt.close("all")

mtcars = pd.read_csv('mtcars.csv')

mtcars4=mtcars[mtcars.cyl==4]
mtcars6=mtcars[mtcars.cyl==6]
mtcars8=mtcars[mtcars.cyl==8]

cetiri=sum(mtcars4.mpg)/len(mtcars4.mpg)
print(cetiri)
sest=sum(mtcars6.mpg)/len(mtcars6.mpg)
print(sest)
osam=sum(mtcars8.mpg)/len(mtcars8.mpg)
print(osam)
x=[4,6,8]
y=[cetiri, sest, osam]      #srednje vrijednosti potrosnji automobila po broju cilindara
plt.figure(1)
plt.grid()
plt.ylabel('prosjecna potrosnja u mpg')
plt.xlabel('broj cilindara')
for i in range (3):
    plt.bar(x[i], y[i])

m4=mtcars[mtcars.cyl==4].wt
m6=mtcars[mtcars.cyl==6].wt
m8=mtcars[mtcars.cyl==8].wt
data=[m4,m6,m8]         
                        #distribucija težina automobila po broju cilindara
plt.figure(2)
plt.boxplot(data)
plt.axis([0, 4, 0, 6])

for i in range (3):
    k=mtcars.groupby('cyl')
print(k.mean())

nmtcars=mtcars
nmtcars=nmtcars.sort('mpg')     #sortiranje po potrosnji
#print(nmtcars)
manual=nmtcars[nmtcars.am==1].mpg   #odvajanje automobila po nacinu prijenosa manual-rucni;auto-automatski
auto=nmtcars[nmtcars.am==0].mpg
  
#mpgs=nmtcars.mpg
#trans=nmtcars.am

#x=np.zeros((len(mtcars)-1,), dtype=np.int)
#for i in range(len(mtcars)):
#   x[i-1]=i

plt.figure(3)
m,=plt.plot(manual)     #prikaz vrijednosti potrosnje u mpg za pojedinu vrstu automobila
a,=plt.plot(auto)       #graf auta s rucnim mjenjacem je kraci zbog manjeg broja podataka tj takvih automobila


#nmtcars=nmtcars.sort('drat')
hpm=nmtcars[nmtcars.am==1].hp    #izdvajanje vrijednosti konjskih snaga i ubrzanja po nacinu prijenosa
hpa=nmtcars[nmtcars.am==0].hp
accm=nmtcars[nmtcars.am==1].drat
acca=nmtcars[nmtcars.am==0].drat
mm=hpm/accm                     #omjeri ks/ubrzanje
aa=hpa/acca
#plt.figure(4)
omm,=plt.plot(mm)       #prikaz vrijednosti omjera
oma,=plt.plot(aa)
plt.legend([m,a,omm,oma], ['Rucni mjenjac mpg', 'Automatski mjenjac mpg','Omjer hp/acc rucni', 'Omjer hp/acc automatski'])
plt.grid()

####drugi nacin prikaza odnosa konjskih snaga i ubrzanja
plt.figure(4)
for i in range (1, len(nmtcars)):
    if(mtcars.am[i]==1):
        man,=plt.plot(mtcars.hp[i], mtcars.drat[i], 'bo', color='r')
    else:
        aut,=plt.plot(mtcars.hp[i], mtcars.drat[i], 'bo', color='b')
plt.legend([man, aut], ['Rucni mjenjac', 'Automatski mjenjac'])
plt.xlabel('Konjske snage automobila')
plt.ylabel('Ubrzanje automobila')

#plt.plot(aa)
#for i in range (1, len(nmtcars)):
#    if(trans[i]==1):
#        plt.bar(i, mpgs[i], color='r')
#    else:
#        plt.bar(i,mpgs[i], color='b')
        

#drat-ubrzanje?

#srt=alles.sort(mpg, 'asc')
#data=np.concatenate((mtcars4, mtcars6, mtcars8),0)
#plt.boxplot(data)
#sortcars.iteritems()
#for item in sortcars.iteritems():
#    print("IDE ITEM", item)
#    
#plt.bar(sortcars.cyl, sortcars.mpg, 0.2, alpha=0.4)


