# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 08:19:00 2015

@author: matej
"""

import pandas as pd

mtcars = pd.read_csv('mtcars.csv')
new_mtcars=mtcars.sort('mpg')
print(new_mtcars.head(5).car)       #vodecih 5 automobila po potrosnji

new_mtcars=mtcars[mtcars.cyl==8]
new_mtcars=new_mtcars.sort('mpg')
new_mtcars=new_mtcars.tail(3)       #zadnja 3 8cilindricna automobila po potrosnji
print (new_mtcars.car)  

new_mtcars=mtcars[mtcars.cyl==8]
mpg=sum(new_mtcars[mtcars.cyl==8].mpg)/len(new_mtcars[mtcars.cyl==8])
print("Prosjecna potrosnja svih automobila s 8 cilindara: ", mpg, "mpg")

new_mtcars=mtcars[(mtcars.cyl==4) & (mtcars.wt>=2.000) & (mtcars.wt<=2.200)]    #selekcija odgovarajucih modela
mpg=sum(new_mtcars.mpg)/len(new_mtcars)
print("Prosjecna potrosnja automobila s 4 cilindra izmedu 2 i 2.2 kpounds: ",mpg,"mpg")

ruc=len(mtcars[mtcars.am==1])
aut=len(mtcars[mtcars.am==0])
print("Postoji", ruc, "automobila s rucnim i ", aut, "automobila s automatskim mjenjacem")

aut=len(mtcars[(mtcars.am==0) & (mtcars.hp>100)])
print("Postoji", aut, "automobila s automatskim mjenjacem i hp>100")

mtcars.wt=mtcars.wt*1000/2.2        #konverzije iz klb u kg
new_mtcars=pd.DataFrame(mtcars, columns=['car', 'wt'])
print (new_mtcars)
